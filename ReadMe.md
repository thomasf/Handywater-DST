## Read Me

This repo contains the core module of the Handywater DST. 


### Models
Currently, the following models are available:
- 1D single Kc approach
- 1D dual Kc approach
- Soil water balance model using historic climate data or weather forecast


### Used data
Basic information about test and valiation sites is stored in sites.csv.
Sensor data needed for calculation of irrigation recommendation in retrieved from the UFZ DMP. 
In case no sensors are installed, weather forecast data can be used.
Coefficients and historic evapotranspiration values for all sites are stored in the coefficients folder. 
