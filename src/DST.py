# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 16:37:22 2022

Try to create an class for DST

@author: thomasf
"""
import base64
import requests
import datetime
import pandas as pd
import pyet
import numpy as np
import math
import time
import warnings
import os
from datetime import date

class dst:
    
    def __init__(self, site, start_date = None, end_date = None):
        '''
        initialize and check if given site is available
        '''
        self.start_date = start_date
        self.end_date = end_date
        sites = pd.read_csv("Sites.csv")
        #if site in sites["Site"].unique():
        if site in set(sites["Site"]):
            self.site = site
        else:
            raise Exception("Unknown site")
    
    def station(self):
        '''
        Assign weather station at DMP for site from look up table
        '''
        sites = pd.read_csv("Sites.csv")
        ID = str(round(sites.loc[sites['Site'] == self.site, 'Station'].iloc[0]))
        return(ID)
            
    def latitude(self):
        '''
        Returns
        -------
        latitude depending on site from look up table 
        '''
        sites = pd.read_csv("Sites.csv")
        lat = str(sites.loc[sites['Site'] == self.site, 'Latitude'].iloc[0])
        return lat
    
    def longitude(self):
        '''
        Returns
        -------
        latitude depending on site from look up table 
        '''
        sites = pd.read_csv("Sites.csv")
        lat = str(sites.loc[sites['Site'] == self.site, 'Longitude'].iloc[0])
        return lat
            
    def altitude(self):
        '''
        Returns
        -------
        altitude depending on site from look up table
        '''
        sites = pd.read_csv("Sites.csv")
        alt = str(sites.loc[sites['Site'] == self.site, 'Altitude'].iloc[0])            
        return alt
    
    def start_day(self):
        '''
        Returns
        -------
        start_day : first day of observation, either as specified or begins one week ago
        '''
        if self.start_date == None:
            start_date = self.end_day() - datetime.timedelta(days=6)
        else:
            start_date = self.start_date
        return start_date

    def end_day(self):
        '''
        Returns
        -------
        end_day : last day of observation, either specified or yesterday
        '''
        if self.end_date == None:
            end_date = datetime.date.today()-datetime.timedelta(days=1)
        else:
            end_date = self.end_date
        return end_date
        
    def req_body_sm(self, target):
        '''

        Parameters
        ----------
        target : target parameter (SoilMoisture - number) to receive from DMP

        Returns
        -------
        req_body : dict to use as request body for post request to DMP

        '''
        # 1h offset during winter time
        if time.localtime().tm_isdst == 0:
            req_body = {"range":{"from":str(self.start_day()) + "T01:00",
                                 "to":str(self.end_day() + 
                                 datetime.timedelta(days=1)) + "T00:59"},
                        # get target values
                         "targets":[{"target":str(target),
                                     "type":"table"}],
                         #"timezone":"Europe/Madrid",
                         "timezone": "UTC",
                         # maxDataPoints should be bigger than actual points
                         "maxDataPoints":10000000000}
            
        # 2h offset during daylight saving time    
        else:
            req_body = {"range":{"from":str(self.start_day()) + "T02:00",
                                 "to":str(self.end_day() + 
                                          datetime.timedelta(days=1)) + "T01:59"},
                        # get target values
                         "targets":[{"target":str(target),
                                     "type":"table"}],
                         #"timezone":"Europe/Madrid",
                         "timezone": "UTC",
                         # maxDataPoints should be bigger than actual points
                         "maxDataPoints":10000000000}
        
        return req_body
    
    def req_body_climate(self, target):
        '''

        Parameters
        ----------
        target : target parameter to receive from DMP
                Temperature: temp_out
                precipitation: precipitation
                wind: wind_speed_avg
                humidity: hum_out
                solar radiation: solar_rad_avg

        Returns
        -------
        req_body : dict to use as request body for post request to DMP

        '''
        # 1h offset during winter time
        if time.localtime().tm_isdst == 0:
            req_body = {"range":{"from":str(self.start_day()-datetime.timedelta(days = 1)) 
                                 + "T23:01",
                                 "to":str(self.end_day()) + "T22:59"},
                        # get target values
                         "targets":[{"target":str(target),
                                     "type":"table"}],
                         #"timezone":"Europe/Madrid",
                         "timezone": "UTC",
                         # maxDataPoints should be bigger than actual points
                         "maxDataPoints":10000000000}
            
        # 2h offset during daylight saving time    
        else:
            req_body = {"range":{"from":str(self.start_day()-datetime.timedelta(days = 1)) 
                                 + "T22:01",
                                 "to":str(self.end_day()) + "T21:59"},
                        # get target values
                         "targets":[{"target":str(target),
                                     "type":"table"}],
                         #"timezone":"Europe/Madrid",
                         "timezone": "UTC",
                         # maxDataPoints should be bigger than actual points
                         "maxDataPoints":10000000000}
        
        return req_body
    
    # def req_body_climate(self, target):
    #     '''

    #     Parameters
    #     ----------
    #     target : target parameter to receive from DMP

    #     Returns
    #     -------
    #     req_body : dict to use as request body for post request to DMP

    #     '''
    #     req_body = {"range":{"from":str(self.start_day()) + "T00:00",
    #                          "to":str(self.end_day()) + "T23:59"},
    #                 # get target values
    #                  "targets":[{"target":str(target),
    #                              "type":"table"}],
    #                  #"timezone":"Europe/Madrid",
    #                  "timezone": "UTC",
    #                  # maxDataPoints should be bigger than actual points
    #                  "maxDataPoints":10000000000}             
    #     return req_body
    
    # def get_aggregata_sm(self, logger_id, target):
    #     '''
    #     Parameters
    #     ----------
    #     logger_id : DMP ID of desired logger
    #     target : desired parameter (SoilMoisture1, SoilMoisture2, ...)

    #     Returns
    #     -------
    #     df with soil moisture values retrieved from DMP
    #     '''
    #     # build query URL
    #     url = 'https://web.app.ufz.de/rdm/aggregata/lvl1/'+logger_id+'/query'
    #     # pass req_body to local variable
    #     #req_body = target
    #     req_body = self.req_body_sm(target)
    #     # create token with Login-credentials from DB-Queryfile
    #     to_code = "DMP_DP649:"+ os.environ["dmp_key"]
    #     token = base64.b64encode(to_code.encode())
    #     token = token.decode()
    #     # specifiy parameters
    #     con_type = 'application/json; charset=utf-8'
    #     headers = {'content-type': con_type, 
    #                 'Authorization': 'Basic ' + token}
    #     # fetch data
    #     response = requests.post(url = url, headers = headers, json = req_body)
    #     # extract information as pd.df
    #     values_df = response.json()
    #     values_df = pd.DataFrame(values_df[0]['rows'], columns = ["Timestamp", target])
    #     values_df['Timestamp'] = pd.to_datetime((values_df['Timestamp']), unit = "ms")
    
    #     # set response for success/failure
    #     if response.status_code == 200:
    #         #print("sucessfully fetched the data")
    #         return(values_df)
    #         # else print error message for clarification 
    #     else:
    #         raise(ValueError(f"There's an error with your request: {response.content}"))    
            
    
    def get_aggregata_climate(self, logger_id, target):
        '''
        Parameters
        ----------
        logger_id : DMP ID of desired logger
        target : desired parameter (Temperature_out, wind_speed_avg
                                    hum_out, solar_rad_avg)

        Returns
        -------
        df with values retrieved from DMP
        '''
        # build query URL
        url = 'https://web.app.ufz.de/rdm/aggregata/lvl1/'+logger_id+'/query'
        # pass req_body to local variable
        #req_body = target
        req_body = self.req_body_climate(target)
        # create token with Login-credentials from DB-Queryfile
        to_code = "DMP_DP649:"+ os.environ["dmp_key"]
        token = base64.b64encode(to_code.encode())
        token = token.decode()
        # specifiy parameters
        con_type = 'application/json; charset=utf-8'
        headers = {'content-type': con_type, 
                    'Authorization': 'Basic ' + token}
        # fetch data
        response = requests.post(url = url, headers = headers, json = req_body)
        # extract information as pd.df
        values_df = response.json()
        values_df = pd.DataFrame(values_df[0]['rows'], columns = ["Timestamp", target])
        values_df['Timestamp'] = pd.to_datetime((values_df['Timestamp']), unit = "ms")
        # convert to metz
        values_df['Timestamp'] = values_df['Timestamp'].dt.tz_localize('UTC').dt.tz_convert('Europe/Madrid')

        # set response for success/failure
        if response.status_code == 200:
            #print("sucessfully fetched the data")
            return(values_df)
            # else print error message for clarification 
        else:
            raise(ValueError(f"There's an error with your request: {response.content}"))  
                    
    
    def get_aggregata_sm(self, logger_id, target):
        '''
        Parameters
        ----------
        logger_id : DMP ID of desired logger
        target : desired parameter (SoilMoisture1, SoilMoisture2, ...)

        Returns
        -------
        df with soil moisture values retrieved from DMP, lv2 (quality checked)
        '''
        # build query URL
        url = 'https://web.app.ufz.de/rdm/aggregata/lvl2/'+logger_id+'/query'
        # pass req_body to local variable
        req_body = self.req_body_sm(target)
        # create token with Login-credentials from DB-Queryfile
        to_code = "DMP_DP649:"+ os.environ["dmp_key"]
        token = base64.b64encode(to_code.encode())
        token = token.decode()
        # specifiy parameters
        con_type = 'application/json; charset=utf-8'
        headers = {'content-type': con_type, 
                    'Authorization': 'Basic ' + token}
        # fetch data
        response = requests.post(url = url, headers = headers, json = req_body)
        # set response for success/failure
        if response.status_code != 200:
            raise(ValueError(f"There's an error with your request: {response.content}")) 
        
        # extract information as pd.df
        values_df = response.json()
        # get colnames in list
        cols = []
        for i in range(len(values_df[0]['columns'])):
            cols.append(values_df[0]['columns'][i]["text"])
        #create pd df    
        values_df = pd.DataFrame(values_df[0]['rows'], columns = cols) 
        # select cols of interest
        values_df = values_df[["TIME", cols[1], cols[2]]].copy()
        # convert time ## get tz sensitive format from DMP?
        values_df['Timestamp'] = pd.to_datetime((values_df['TIME']), unit = "ms")
        # convert from UTC to Middle european time
        #values_df['Timestamp'] = values_df['Timestamp'].dt.tz_localize('UTC').dt.tz_convert('Europe/Madrid')
        # check if data quality is ok
        values_df_ok = values_df.loc[values_df["QUALITY_FLAG"] == "OK"]
        # focus on time and measured value
        values_df_ok = values_df_ok[["Timestamp", cols[1]]]
        # rename 
        values_df_ok.rename(columns = {cols[1] : target}, inplace = True)
        # print warning if values have been removed
        if len(values_df) != len(values_df_ok):
            warnings.warn("Bad quality data has been removed")
        return(values_df_ok)
  
    # def get_aggregata_climate(self, logger_id, target):
    #     '''
    #     Parameters
    #     ----------
    #     logger_id : DMP ID of desired logger
    #     target : desired parameter (Temperature_out, wind_speed_avg
    #                                 hum_out, solar_rad_avg)

    #     Returns
    #     -------
    #     df with climate data values retrieved from DMP, lv2 (quality checked)
    #     '''
    #     # build query URL
    #     url = 'https://web.app.ufz.de/rdm/aggregata/lvl2/'+logger_id+'/query'
    #     # pass req_body to local variable
    #     req_body = self.req_body_climate(target)
    #     # create token with Login-credentials from DB-Queryfile
    #     to_code = "DMP_DP649:"+ os.environ["dmp_key"]
    #     token = base64.b64encode(to_code.encode())
    #     token = token.decode()
    #     # specifiy parameters
    #     con_type = 'application/json; charset=utf-8'
    #     headers = {'content-type': con_type, 
    #                 'Authorization': 'Basic ' + token}
    #     # fetch data
    #     response = requests.post(url = url, headers = headers, json = req_body)
    #     # set response for success/failure
    #     if response.status_code != 200:
    #         raise(ValueError(f"There's an error with your request: {response.content}")) 
        
    #     # extract information as pd.df
    #     values_df = response.json()
    #     # get colnames in list
    #     cols = []
    #     for i in range(len(values_df[0]['columns'])):
    #         cols.append(values_df[0]['columns'][i]["text"])
    #     #create pd df    
    #     values_df = pd.DataFrame(values_df[0]['rows'], columns = cols) 
    #     # select cols of interest
    #     values_df = values_df[["TIME", cols[1], cols[2]]].copy()
    #     # convert time ## get tz sensitive format from DMP?
    #     values_df['Timestamp'] = pd.to_datetime((values_df['TIME']), unit = "ms")
    #     # check if data quality is ok
    #     values_df_ok = values_df.loc[values_df["QUALITY_FLAG"] == "OK"]
    #     # focus on time and measured value
    #     values_df_ok = values_df_ok[["Timestamp", cols[1]]]
    #     # rename 
    #     values_df_ok.rename(columns = {cols[1] : target}, inplace = True)
    #     # print warning if values have been removed
    #     if len(values_df) != len(values_df_ok):
    #         warnings.warn("Bad quality data has been removed")
    #     return(values_df_ok)


    def hargreaves_et(self):
        '''
        Returns
        -------
        et : Evapotranspiration for selected timerange per day based on 
        hargreaves formula using historic solar radiation values
        '''
        # store look up table for solar radiation
        sr = pd.DataFrame({'month': {0: 1,
          1: 2,
          2: 3,
          3: 4,
          4: 5,
          5: 6,
          6: 7,
          7: 8,
          8: 9,
          9: 10,
          10: 11,
          11: 12},
         'R0': {0: 6.4,
          1: 8.6,
          2: 11.3,
          3: 14.3,
          4: 16.3,
          5: 17.1,
          6: 16.7,
          7: 15.1,
          8: 12.4,
          9: 9.4,
          10: 6.9,
          11: 5.8}})
        
        # # R0 for DS2
        # sr = pd.DataFrame({'month': {0: 1,
        #   1: 2,
        #   2: 3,
        #   3: 4,
        #   4: 5,
        #   5: 6,
        #   6: 7,
        #   7: 8,
        #   8: 9,
        #   9: 10,
        #   10: 11,
        #   11: 12},
        #  'R0': {0: 6.85,
        #   1: 9,
        #   2: 11.65,
        #   3: 14.5,
        #   4: 16.3,
        #   5: 17.05,
        #   6: 16.7,
        #   7: 15.2,
        #   8: 12.7,
        #   9: 9.8,
        #   10: 7.35,
        #   11: 6.3}}) 
                
        # get sr value for each day
        daterange = pd.date_range(start = self.start_day(), 
                                  end = self.end_day()) 
        R0 = []
        for d in daterange:
            month = d.month
            for i in range (0, 12):
                if i+1 == month:
                    R0d = sr["R0"][i+1]
                    R0.append(R0d)
        R0 = pd.DataFrame({'R0': R0}, index = daterange)
        tmean, tmin, tmax = self.temperature()
        # store values in df
        day_temp = pd.DataFrame({"tmin":tmin.Temperature_out, 
                              "tmean":tmean.Temperature_out, 
                              "tmax":tmax.Temperature_out})
        # separate
        tmin, tmean, tmax = [day_temp[col] for col in day_temp.columns]
        # calculate Hargreaves et
        et = pd.DataFrame({"ET_Hargreaves" : 0.0023*(tmean + 17.78)*R0.R0*(tmax - tmin) ** 0.5})
        return et
    
    def hargreaves_et_forecast(self):
        '''
        function calculates forecast ET based on hargeaves formula with historic
        radiation values and forecast data from openmeteo
        
        Returns
        -------
        et : Evapotranspiration for selected timerange per day based on 
        hargreaves formula using forecast solar radiation values
        '''
        # store look up table for solar radiation
        sr = pd.DataFrame({'month': {0: 1,
          1: 2,
          2: 3,
          3: 4,
          4: 5,
          5: 6,
          6: 7,
          7: 8,
          8: 9,
          9: 10,
          10: 11,
          11: 12},
         'R0': {0: 6.4,
          1: 8.6,
          2: 11.3,
          3: 14.3,
          4: 16.3,
          5: 17.1,
          6: 16.7,
          7: 15.1,
          8: 12.4,
          9: 9.4,
          10: 6.9,
          11: 5.8}})
        
        # get sr value for each day
        # daterange today + 7
        daterange = pd.date_range(start = self.end_day() + datetime.timedelta(days = 1), 
                                          end = self.end_day() + datetime.timedelta(days = 7)) 
        
        R0 = []
        for d in daterange:
            month = d.month
            for i in range (0, 12):
                if i+1 == month:
                    R0d = sr["R0"][i]
                    R0.append(R0d)
        R0 = pd.DataFrame({'R0': R0}, index = daterange)
        tmean, tmin, tmax = self.temperature_forecast()
        # store values in df
        day_temp = pd.DataFrame({"tmin":tmin.temperature_2m, 
                              "tmean":tmean.temperature_2m, 
                              "tmax":tmax.temperature_2m})
        # separate
        tmin, tmean, tmax = [day_temp[col] for col in day_temp.columns]
        # calculate Hargreaves et
        et = pd.DataFrame({"ET_Hargreaves" : 0.0023*(tmean + 17.78)*R0.R0*(tmax - tmin) ** 0.5})
        return et
    
    def temperature(self):
        '''
        Returns
        -------
        day_temp_mean : mean temperature per day
        day_temp_min : min temperature per day
        day_temp_max : max temperature per day
        '''
        temp = self.get_aggregata_climate(self.station(), 
                                  "Temperature_out")  
        # group by day(mean) and add min and max temp values per day
        day_temp_mean = temp.groupby(temp.Timestamp.dt.date).mean(numeric_only = True)
        day_temp_min = temp.groupby(temp.Timestamp.dt.date).min()
        day_temp_max = temp.groupby(temp.Timestamp.dt.date).max()
        
        #daterange = pd.date_range(start = self.start_day(), 
         #                                 end = self.end_day()).strftime('%Y-%m-%d')
        
        return day_temp_mean, day_temp_min, day_temp_max
    
    def temperature_forecast(self):
        '''
        get weather forecast data for 7 days from openmeteo 

        Returns
        -------
        day_temp_mean : forecast temperature mean
        day_temp_min : forecast temperature min
        day_temp_max : forecast temperature max
        '''
        # openweather baseurl
        #base = "https://api.open-meteo.com/v1/forecast?"
        base = "https://api.open-meteo.com/v1/ecmwf?"
        # specify geolocation
        specurl = "latitude="+self.latitude()+"&longitude="+self.longitude() 
        # set start/end_day
        start_day = '&start_date=' + str(self.end_day() + datetime.timedelta(days = 1))
        end_day = '&end_date=' + str(self.end_day() + datetime.timedelta(days = 7))

        # fetch open forecast
        url = (base + specurl + 
               start_day + 
               end_day +
               '&hourly=temperature_2m&timeformat=unixtime')
              #'&hourly=temperature_2m&timeformat=unixtime&timezone=Etc/UTC')
        
        response = requests.get(url = url)
        # extract information as pd.df
        values_df = response.json()
        temp  = pd.DataFrame(values_df['hourly'])
        temp.time = pd.to_datetime(temp['time'], unit='s', utc = True)
        
        # group by day(mean) and add min and max temp values per day
        day_temp_mean = temp.groupby(temp.time.dt.date).mean(numeric_only = True)
        day_temp_min = temp.groupby(temp.time.dt.date).min()
        day_temp_max = temp.groupby(temp.time.dt.date).max()
        return day_temp_mean, day_temp_min, day_temp_max

    def wind(self):
        '''
        Returns 
        -------
        day_wind : average wind speed per day
        '''
        # Wind
        wind = self.get_aggregata_climate(self.station(), 
                                  "wind_speed_avg")
        # convert to meter/second
        wind.wind_speed_avg = round(wind.wind_speed_avg/2.236936, 1)
        # group by day(mean) 
        day_wind = wind.groupby(wind.Timestamp.dt.date).mean(numeric_only = True)
        return day_wind
    
    def wind_forecast(self):
        '''
        get weather forecast data for 7 days from openmeteo
        wind: windspeed_10m
        
        Returns
        -------
        variable : wind forecast 
        '''
        # openweather baseurl
        #base = "https://api.open-meteo.com/v1/forecast?"
        base = "https://api.open-meteo.com/v1/ecmwf?"
        # specify geolocation
        specurl = "latitude="+self.latitude()+"&longitude="+self.longitude() 
        # set start/end_day
        start_day = '&start_date=' + str(self.end_day() + datetime.timedelta(days = 1))
        end_day = '&end_date=' + str(self.end_day() + datetime.timedelta(days = 7))

        # fetch open forecast
        url = (base + specurl + 
               start_day + 
               end_day +
               '&hourly=windspeed_10m&windspeed_unit=ms&timeformat=unixtime')
               #'&hourly=windspeed_10m&windspeed_unit=ms&timeformat=unixtime&timezone=Etc/UTC')
        response = requests.get(url = url)
        # extract information as pd.df
        values_df = response.json()
        var  = pd.DataFrame(values_df['hourly'])
        var.time = pd.to_datetime(var['time'], unit='s', utc = True)
        # group by day(mean)
        day_var_mean = var.groupby(var.time.dt.date).mean(numeric_only = True)
        return day_var_mean
    
    # def climate_forecast(self, variable):
    #     # currently only used for humidity
    #     '''
    #     get weather forecast data for 7 days from openmeteo
    #     wind: windspeed_10m
    #     humidity: relativehumidity_2m
        
    #     Returns
    #     -------
    #     variable : forecast variable
    #     '''
    #     # openweather baseurl
    #     base = "https://api.open-meteo.com/v1/forecast?"
    #     # specify geolocation
    #     specurl = "latitude="+self.latitude()+"&longitude="+self.longitude() 
    #     # set start/end_day
    #     start_day = '&start_date=' + str(self.end_day() + datetime.timedelta(days = 1))
    #     end_day = '&end_date=' + str(self.end_day() + datetime.timedelta(days = 7))

    #     # fetch open forecast
    #     url = (base + specurl + 
    #            start_day + 
    #            end_day +
    #           '&hourly='+variable+'&timeformat=unixtime&timezone=Etc/UTC')
    #     response = requests.get(url = url)
    #     # extract information as pd.df
    #     values_df = response.json()
    #     var  = pd.DataFrame(values_df['hourly'])
    #     var.time = pd.to_datetime(var['time'], unit='s', utc = True)
    #     # group by day(mean)
    #     day_var_mean = var.groupby(var.time.dt.date).mean(numeric_only = True)
        
    #     return day_var_mean   
 
    def humidity_forecast(self, variable):
        # currently only used for humidity
        '''
        get weather forecast data for 7 days from openmeteo
        humidity: relativehumidity_2m
        
        Returns
        -------
        variable : forecast of humidity 
        '''
        # openweather baseurl
        #base = "https://api.open-meteo.com/v1/forecast?"
        base = "https://api.open-meteo.com/v1/ecmwf?"
        # specify geolocation
        specurl = "latitude="+self.latitude()+"&longitude="+self.longitude() 
        # set start/end_day
        start_day = '&start_date=' + str(self.end_day() + datetime.timedelta(days = 1))
        end_day = '&end_date=' + str(self.end_day() + datetime.timedelta(days = 7))

        # fetch open forecast
        url = (base + specurl + 
               start_day + 
               end_day +
               '&hourly='+variable+'&timeformat=unixtime')
              #'&hourly='+variable+'&timeformat=unixtime&timezone=Etc/UTC')
        response = requests.get(url = url)
        # extract information as pd.df
        values_df = response.json()
        hum  = pd.DataFrame(values_df['hourly'])
        hum.time = pd.to_datetime(hum['time'], unit='s', utc = True)
        
        # group by day       
        day_hum_mean = hum.groupby(hum.time.dt.date).mean(numeric_only = True)
        day_hum_min = hum.groupby(hum.time.dt.date).min()
        day_hum_max = hum.groupby(hum.time.dt.date).max()
        return day_hum_mean, day_hum_min, day_hum_max
    
    def humidity(self):
        '''
        Returns
        -------
        day_hum : measured average humidity per day
        '''
        # humidity
        hum = self.get_aggregata_climate(self.station(), 
                                  "hum_out")  
        # group by day(mean) 
        day_hum_mean = hum.groupby(hum.Timestamp.dt.date).mean(numeric_only = True)
        day_hum_min = hum.groupby(hum.Timestamp.dt.date).min()
        day_hum_max = hum.groupby(hum.Timestamp.dt.date).max()
        return day_hum_mean, day_hum_min, day_hum_max
    
    def solar_rad(self):
        '''
        Returns
        -------
        day_rad : measured solar radiation per day in MJ/m²
        '''
        # solar radiation
        rad = self.get_aggregata_climate(self.station(), 
                                  "solar_rad_avg")  
        # group by day(mean) 
        day_rad = rad.groupby(rad.Timestamp.dt.date).mean(numeric_only = True)
        day_rad = day_rad*24*60*60/1000000 # in MJ/m²
        return day_rad
    
    def solar_rad_forecast(self):
        '''
        get weather forecast data for 7 days from openmeteo
        temperature: temperature_2m
        wind: windspeed_10m
        humidity: relativehumidity_2m
        radiation: diffuse_radiation, direct_radiation
        
        Returns
        -------
        variable : forecast solar radiation
        '''
        # openweather baseurl
        base = "https://api.open-meteo.com/v1/forecast?"
        # specify geolocation
        specurl = "latitude="+self.latitude()+"&longitude="+self.longitude() 
        # set start/end_day
        start_day = '&start_date=' + str(self.end_day() + datetime.timedelta(days = 1))
        end_day = '&end_date=' + str(self.end_day() + datetime.timedelta(days = 7))

        # fetch open forecast
        url = (base + specurl + 
               start_day + 
               end_day+
              '&daily=shortwave_radiation_sum&timeformat=unixtime&timezone=Etc/UTC')
        response = requests.get(url = url)
        # extract information as pd.df
        values_df = response.json()
        var  = pd.DataFrame(values_df['daily'])
        var.time = pd.to_datetime(var['time'], unit='s', utc = True)
        # group by day(mean)
        day_var_mean = var.groupby(var.time.dt.date).mean(numeric_only = True)
        return day_var_mean
    
    def get_climate_data(self):
        '''
        queries all climate variables

        Returns
        -------
        pandas DF with climate variables for daterange, including ETo values per day
        '''
        # calculate ET0 after FAO56
        tmean, tmin, tmax = self.temperature()
        wind = self.wind()
        hum_mean, hum_min, hum_max = self.humidity()
        rad = self.solar_rad()
        eto = self.penman_et()
        # store needed data in one df
        meteo = pd.DataFrame({"time":tmean.index, 
                              "tmean":tmean.Temperature_out, 
                              "tmax":tmax.Temperature_out, 
                              "tmin":tmin.Temperature_out, 
                              "rh":hum_mean.hum_out, 
                              "rhmin":hum_min.hum_out, 
                              "rhmax":hum_max.hum_out, 
                              "wind":wind.wind_speed_avg, 
                              "rs":rad.solar_rad_avg})
        meteo["eto"] = eto.PM_FAO_56
        return(meteo)

    def penman_et(self):
        '''
        calculates ET based on Penman-Monteith

        Returns
        -------
        Et values per day, based on measured values
        '''
        # calculate ET0 after FAO56
        tmean, tmin, tmax = self.temperature()
        wind = self.wind()
        hum_mean, hum_min, hum_max = self.humidity()
        rad = self.solar_rad()
        lat = float(self.latitude())*np.pi/180  # Latitude of the meteorological station, converting from degrees to radians
        elevation = float(self.altitude())  # meters above sea-level
        # store needed data in one df
        meteo = pd.DataFrame({"time":tmean.index, 
                              "tmean":tmean.Temperature_out, 
                              "tmax":tmax.Temperature_out, 
                              "tmin":tmin.Temperature_out, 
                              "rh":hum_mean.hum_out, 
                              "rhmin":hum_min.hum_out, 
                              "rhmax":hum_max.hum_out, 
                              "wind":wind.wind_speed_avg, 
                              "rs":rad.solar_rad_avg}) # in MJ/m²
        # set datetime index
        meteo.index = pd.to_datetime(meteo.time)

        # get single objects to pass to function
        time, tmean, tmax, tmin, rh, rhmin, rhmax, wind, rs = [meteo[col] for col in meteo.columns]

        # Estimate evapotranspiration 
        penman_et = pyet.pm_fao56(tmean, wind, rs=rs, elevation=elevation,
                                       lat=lat, tmax=tmax, tmin=tmin, 
                                       rh=rh, rhmin = rhmin, rhmax = rhmax)
        return(pd.DataFrame(penman_et))
    
    def penman_forecast(self):
        '''

        Returns
        -------
        forecast ETo values per day
        
        #OWM reports wind speed at 10m height, so need to convert to 2m:
               data["wind_speed"] = data["wind_speed"] * (4.87 / math.log((67.8 * 10) - 5.42))
        
        '''
        # calculate ET0 after FAO56
        tmean, tmin, tmax = self.temperature_forecast()
        wind = self.wind_forecast()
        # convert to 2m
        wind = wind * (4.87 / math.log((67.8 * 10) - 5.42))
        hum_mean, hum_min, hum_max = self.humidity_forecast("relativehumidity_2m")
        rad = self.solar_rad_forecast()
        lat = float(self.latitude())*np.pi/180  # Latitude of the meteorological station, converting from degrees to radians
        elevation = float(self.altitude())  # meters above sea-level
        # store needed data in one df
        meteo = pd.DataFrame({"time":tmean.index, 
                              "tmean":tmean.temperature_2m, 
                              "tmax":tmax.temperature_2m, 
                              "tmin":tmin.temperature_2m, 
                              "rh_mean":hum_mean.relativehumidity_2m,
                              "rh_min":hum_min.relativehumidity_2m,
                              "rh_max":hum_max.relativehumidity_2m,                              
                              "wind":wind.windspeed_10m, 
                              "rs":rad.shortwave_radiation_sum}) # in MJ/m²
        # set datetime index
        meteo.index = pd.to_datetime(meteo.time)

        # get single objects to pass to function
        time, tmean, tmax, tmin, rh_mean, rh_min, rh_max, wind, rs = [meteo[col] for col in meteo.columns]

        # Estimate evapotranspiration 
        penman_et = pyet.pm_fao56(tmean, wind, rs=rs, elevation=elevation,
                                        lat=lat, tmax=tmax, tmin=tmin, 
                                        rh =rh_mean)# rhmin = rh_min, rhmax = rh_max)
        return(pd.DataFrame(penman_et))
        
    def Kc(self):
        '''
        function to receive Kc values from look up table
        
        Returns
        -------
        Kc values corresponding to selected study site and month per day
        '''
        try:
            fields = ['Month', 'Kc']
            Kc_data = pd.read_csv("coefficients/"+ self.site +"_Kc.csv", usecols=fields)
            daterange = pd.date_range(start = self.start_day(), 
                                              end = self.end_day())
            Kc = []
            for d in daterange:
                month = d.month
                for i in range (0, 12):
                    if i+1 == month:
                        Kcd = Kc_data["Kc"][i]
                        Kc.append(Kcd)
            return(pd.DataFrame(Kc, index = daterange))
        # print error message if failing
        except Exception as e:
            print(e)
                
    def FAO_single(self, Et = "penman"):
        '''
        Parameters
        ----------
        Et : evapotranspiration per day:  penman or hargreaves
        options from forecast: openmeteo (direct et value) or ecmwf (calculated based on ecmwf values)

        Returns
        -------
        Sum of Etc for FAO 1D single approach
        '''
    
        # get Kc values
        Kc = self.Kc()

        if Et == "hargreaves":
            Et = self.hargreaves_et()
        elif Et == "penman":
            Et = self.penman_et()
        elif Et == "openmeteo":
            Et = self.penman_et_openmeteo()
        elif Et == "ecmwf":
            Et = self.penman_et_ecmwf()
            #calculate (as numpy due to problems with dfs)
        ETc = pd.DataFrame(Et.to_numpy()*Kc.to_numpy())            
        # sum to get evaporation for whole week in mm/m²
        return round(ETc.sum().item(), 2)
    
    def Kcb(self):
        '''
        function to receive Kcb values for 1D dual approach from look up table
        
        Returns
        -------
        Kcb values corresponding to selected study site and month per day
        '''
        try:
            fields = ['Month', 'Kcb']
            Kc_data = pd.read_csv("coefficients/"+ self.site +"_Kc.csv", usecols=fields)

            # iterating over the dates to assign Kc per day depending on month
            daterange = pd.date_range(start = self.start_day(), 
                                          end = self.end_day())
            Kcb = []
            for d in daterange:
                month = d.month
                for i in range (0, 12):
                    if i+1 == month:
                        Kcbd = Kc_data["Kcb"][i]
                        Kcb.append(Kcbd)
            return(pd.DataFrame(Kcb, index = daterange))
        # print error message if failing
        except Exception as e:
            print(e)

    def Ke(self):
        '''
        function to receive Ke values for 1D dual approach from look up table
        
        Returns
        -------
        Ke values corresponding to selected study site and month per day
        '''
        try:
            fields = ['Month', 'Ke']
            Kc_data = pd.read_csv("coefficients/"+ self.site +"_Kc.csv", usecols=fields)

            # iterating over the dates to assign Kc per day depending on month
            daterange = pd.date_range(start = self.start_day(), 
                                          end = self.end_day())
            Ke = []
            for d in daterange:
                month = d.month
                for i in range (0, 12):
                    if i+1 == month:
                        Ked = Kc_data["Ke"][i]
                        Ke.append(Ked)
            return(pd.DataFrame(Ke, index = daterange))
        # print error message if failing
        except Exception as e:
            print(e)
    
    def FAO_dual(self, Et = "penman"):
        '''
        Parameters
        ----------
        Et : evapotranspiration per day:  penman or hargreaves
        options from forecast: openmeteo (direct et value) or ecmwf (calculated based on ecmwf values)

        Returns
        -------
        Sum of Etc for FAO 1D dual approach
        '''
        # get Kc values
        Kcb = self.Kcb()
        Ke = self.Ke()
        
        if Et == "hargreaves":
            Et = self.hargreaves_et()
        elif Et == "penman":
            Et = self.penman_et()
        elif Et == "openmeteo":
            Et = self.penman_et_openmeteo()
        elif Et == "ecmwf":
            Et = self.penman_et_ecmwf()
        ETc = pd.DataFrame(Et.to_numpy()*(Kcb.to_numpy() + Ke.to_numpy()))            
        # sum to get evaporation for whole week in mm/m²
        return round(ETc.sum().item(), 2)
     
    def fc(self, logger_id, sensor_nr):
        '''
        Parameters
        ----------
        logger_id : DMP logger ID
        
        sensor_nr : DMP Soilmoisture NR

        Returns
        -------
        fc : FC value for corresponding SM sensor from look-up table 

        '''
        try:
            fields = ['DMP_NR', 'SM_NR', "FC"]
            FC_data = pd.read_csv("coefficients/FC_information.csv" , usecols=fields)
            condition = (FC_data["DMP_NR"] == logger_id) & (FC_data["SM_NR"] == sensor_nr)
            fc = FC_data.loc[condition, 'FC'].values[0]
            return fc
        except Exception as e:
            print(e, "FC values not available")
            
    def rwsc(self, logger_id, sensor_nr):
        '''
    
        Parameters
        ----------
        logger_id : TYPE
            DESCRIPTION.
        sensor_nr : TYPE
            DESCRIPTION.

        Returns
        -------
        fc : rwsc value for SM sensor from look-up table 

        '''
        try:
            fields = ['DMP_NR', 'SM_NR', "RWSC"]
            df = pd.read_csv("coefficients/FC_information.csv" , usecols=fields)
            condition = (df["DMP_NR"] == logger_id) & (df["SM_NR"] == sensor_nr)
            rwsc = df.loc[condition, 'RWSC'].values[0]
            return rwsc
        except Exception as e:
            print(e, "rwsc not available")
                    
    def factor_fh(self, treatment):
        
        # logger id. sensor_id, treatment?
        
        '''
        calculates kh factor based on measured soil water content
        treatment 4: historic et values
        treatment 5: forecast et values
        currently, treatment 5 and 4 are on DS1A, treatment 4 is on DS5 and pilotplot
        
        Parameters
        ----------
        treatment : treatment of swbm, 4 (historic) or 5 (weather forecast)

        Raises
        ------
        Error if treatment is not available for selected site

        Returns
        -------
        soil water content adjustment coefficient
        '''
        # select sensors depending on site and treatment
        if self.site == "DS1A":
            # define rws crop for DS1
            rwsc = self.rwsc(1805, 1)
            if treatment == 5:
                # set specific field capacity per sensor
                fc = [float(self.fc(1805, 1)), float(self.fc(1802, 1))]
                # store data from soilmoisture sensors that are used for swbm in pedralba
                data = []
                data.append(self.get_aggregata_sm("1805", "SoilMoisture1"))
                data.append(self.get_aggregata_sm("1802", "SoilMoisture1"))
                # calculate relative water status based on values from both sensors
                dat = []
                last_value = []
                first_value = []
                for i in range(0, len(data)):
                    # rename column to make iteration possible
                    data[i] = data[i].rename(columns = {data[i].columns[1] : "SoilMoisture"})
                    # calculate relative water status by dividing by fc
                    data[i]['RWS'] = data[i]['SoilMoisture']/fc[i]
                    # get min per day and sensor
                    day_min = data[i].groupby(data[i].Timestamp.dt.date).min() 
                    # calculate HnRef
                    first_value.append(day_min['RWS'].iloc[0])
                    last_value.append(day_min['RWS'].iloc[6])
                    # use first and last measurement
                    #first_value.append(data[i]["RWS"][0])
                    #last_value.append(data[i]["RWS"].iloc[-1])
                    diff = (first_value[i] - last_value[i])*100
                    if last_value[i] > rwsc:
                        HnRef = last_value[i]
                    elif diff < 0:
                        HnRef = rwsc
                    else:
                        HnRef = last_value[i] 
                    dat.append(HnRef)
                # calculate mean of values from both sensors
                avg = np.mean(dat)
                # calc kh and return
                return(round(rwsc/avg, 3))
                    
            elif treatment == 4:
                # set specific field capacity per sensor
                fc = [float(self.fc(1804, 3)), float(self.fc(1802, 3))]
                # store data from soilmoisture sensors that are used for swbm in pedralba
                data = []
                data.append(self.get_aggregata_sm("1804", "SoilMoisture3"))
                # beware port 4 == soilmoisture 3
                data.append(self.get_aggregata_sm("1802", "SoilMoisture3"))
                dat = []
                last_value = []
                first_value = []
                for i in range(0, len(data)):
                    # rename column to make iteration possible
                    data[i] = data[i].rename(columns = {data[i].columns[1] : "SoilMoisture"})
                    # calculate relative water status
                    data[i]['RWS'] = data[i]['SoilMoisture']/fc[i]
                    # get min value per day
                    day_min = data[i].groupby(data[i].Timestamp.dt.date).min() 
                    # calculate HnRef
                    first_value.append(day_min['RWS'].iloc[0])
                    last_value.append(day_min['RWS'].iloc[6])
                    # use first and last measurement
                    #first_value.append(data[i]["RWS"][0])
                    #last_value.append(data[i]["RWS"].iloc[-1])
                    diff = (first_value[i] - last_value[i])*100
                    if last_value[i] > rwsc:
                        HnRef = last_value[i]
                    elif diff < 0:
                        HnRef = rwsc
                    else:
                        HnRef = last_value[i] 
                    dat.append(HnRef)
                # calculate mean of values from both sensors
                avg = np.mean(dat)
                # calc kh and return
                return(round(rwsc/avg, 3))
            else:
                raise Exception("Treatment not available for selected site")

        elif self.site == "DS5":
            # define rws crop for DS5
            rwsc = self.rwsc(1816, 1)
            if treatment == 4:
                # set specific field capacity per sensor
                fc = [float(self.fc(1816, 1)), float(self.fc(1809, 3))]
                # store data from soilmoisture sensors that are used for swbm in pedralba
                data = []
                #data.append(self.get_aggregata("1817", "SoilMoisture1"))
                data.append(self.get_aggregata_sm("1816", "SoilMoisture1"))
                data.append(self.get_aggregata_sm("1809", "SoilMoisture3"))
                # calculate relative water status based on values from both sensors
                dat = []
                last_value = []
                first_value = []
                for i in range(0, len(data)):
                    # rename column to make iteration possible
                    data[i] = data[i].rename(columns = {data[i].columns[1] : "SoilMoisture"})
                    # calculate relative water status
                    data[i]['RWS'] = data[i]['SoilMoisture']/fc[i]
                    # get min per day
                    day_min = data[i].groupby(data[i].Timestamp.dt.date).min()
                    # calculate HnRef
                    first_value.append(day_min['RWS'].iloc[0])
                    last_value.append(day_min['RWS'].iloc[6])
                    # use first and last measurement
                    #first_value.append(data[i]["RWS"][0])
                    #last_value.append(data[i]["RWS"].iloc[-1])
                    diff = (first_value[i] - last_value[i])*100
                    if last_value[i] > rwsc:
                        HnRef = last_value[i]
                    elif diff < 0:
                        HnRef = rwsc
                    else:
                        HnRef = last_value[i] 
                    dat.append(HnRef)
                # calculate mean of values from both sensors
                avg = np.mean(dat)   
                # calc kh and return
                return(round(rwsc/avg, 3))
            else:
                raise Exception("Treatment not available for selected site")
                
        elif self.site == "olivepilot":
            # define rws crop for oliveplot
            rwsc = self.rwsc(2146, 1)
            if treatment == 4:
                # set specific field capacity per sensor
                fc = [float(self.fc(2146, 1)), float(self.fc(2146, 3))]
                # store data from soilmoisture sensors that are used for swbm in pedralba
                data = []
                data.append(self.get_aggregata_sm("2146", "SoilMoisture1"))
                data.append(self.get_aggregata_sm("2146", "SoilMoisture3"))
                # calculate relative water status based on values from both sensors
                dat = []
                last_value = []
                first_value = []
                for i in range(0, len(data)):
                    # rename column to make iteration possible
                    data[i] = data[i].rename(columns = {data[i].columns[1] : "SoilMoisture"})
                    # calculate relative water status
                    data[i]['RWS'] = data[i]['SoilMoisture']/fc[i]
                    # get min per day
                    day_min = data[i].groupby(data[i].Timestamp.dt.date).min()
                    # calculate HnRef
                    first_value.append(day_min['RWS'].iloc[0])
                    last_value.append(day_min['RWS'].iloc[6])
                    diff = (first_value[i] - last_value[i])*100
                    if last_value[i] > rwsc:
                        HnRef = last_value[i]
                    elif diff < 0:
                        HnRef = rwsc
                    else:
                        HnRef = last_value[i] 
                    dat.append(HnRef)
                # calculate mean of values from both sensors
                avg = np.mean(dat)   
                # calc kh and return
                return(round(rwsc/avg, 3))
            
        elif self.site == "citruspilot":
            # define rws crop for oliveplot
            rwsc = self.rwsc(2172, 1)
            if treatment == 4:
                # set specific field capacity per sensor
                fc = [float(self.fc(2172, 1)), float(self.fc(2172, 3))]
                # store data from soilmoisture sensors that are used for swbm in pedralba
                data = []
                data.append(self.get_aggregata_sm("2172", "SoilMoisture1"))
                data.append(self.get_aggregata_sm("2172", "SoilMoisture3"))
                # calculate relative water status based on values from both sensors
                dat = []
                last_value = []
                first_value = []
                for i in range(0, len(data)):
                    # rename column to make iteration possible
                    data[i] = data[i].rename(columns = {data[i].columns[1] : "SoilMoisture"})
                    # calculate relative water status
                    data[i]['RWS'] = data[i]['SoilMoisture']/fc[i]
                    # get min per day
                    day_min = data[i].groupby(data[i].Timestamp.dt.date).min()
                    # calculate HnRef
                    first_value.append(day_min['RWS'].iloc[0])
                    last_value.append(day_min['RWS'].iloc[6])
                    diff = (first_value[i] - last_value[i])*100
                    if last_value[i] > rwsc:
                        HnRef = last_value[i]
                    elif diff < 0:
                        HnRef = rwsc
                    else:
                        HnRef = last_value[i] 
                    dat.append(HnRef)
                # calculate mean of values from both sensors
                avg = np.mean(dat)   
                # calc kh and return
                return(round(rwsc/avg, 3))
            
        elif self.site == "DS2_7":
            # define rws crop for DS2_7
            rwsc = self.rwsc(2161, 1)
            if treatment == 4 or treatment == 5:
                # set specific field capacity per sensor
                fc = [float(self.fc(2161, 1)), float(self.fc(2161, 1))]
                # store data from soilmoisture sensors that are used for swbm in pedralba
                data = []
                data.append(self.get_aggregata_sm("2161", "SoilMoisture1"))
                data.append(self.get_aggregata_sm("2161", "SoilMoisture2"))
                # calculate relative water status based on values from both sensors
                dat = []
                last_value = []
                first_value = []
                for i in range(0, len(data)):
                    # rename column to make iteration possible
                    data[i] = data[i].rename(columns = {data[i].columns[1] : "SoilMoisture"})
                    # calculate relative water status
                    data[i]['RWS'] = data[i]['SoilMoisture']/fc[i]
                    # get min per day
                    day_min = data[i].groupby(data[i].Timestamp.dt.date).min()
                    # calculate HnRef
                    first_value.append(day_min['RWS'].iloc[0])
                    last_value.append(day_min['RWS'].iloc[6])
                    diff = (first_value[i] - last_value[i])*100
                    if last_value[i] > rwsc:
                        HnRef = last_value[i]
                    elif diff < 0:
                        HnRef = rwsc
                    else:
                        HnRef = last_value[i] 
                    dat.append(HnRef)
                # calculate mean of values from both sensors
                avg = np.mean(dat)   
                # calc kh and return
                return(round(rwsc/avg, 3))
        
        elif self.site == "DS2_8C":
            # define rws crop for DS2_8C
            rwsc = self.rwsc(2163, 1)
            if treatment == 4 or treatment == 5:
                # set specific field capacity per sensor
                fc = [float(self.fc(2163, 1)), float(self.fc(2163, 1))]
                # store data from soilmoisture sensors that are used for swbm in pedralba
                data = []
                data.append(self.get_aggregata_sm("2163", "SoilMoisture1"))
                data.append(self.get_aggregata_sm("2163", "SoilMoisture2"))
                # calculate relative water status based on values from both sensors
                dat = []
                last_value = []
                first_value = []
                for i in range(0, len(data)):
                    # rename column to make iteration possible
                    data[i] = data[i].rename(columns = {data[i].columns[1] : "SoilMoisture"})
                    # calculate relative water status
                    data[i]['RWS'] = data[i]['SoilMoisture']/fc[i]
                    # get min per day
                    day_min = data[i].groupby(data[i].Timestamp.dt.date).min()
                    # calculate HnRef
                    first_value.append(day_min['RWS'].iloc[0])
                    last_value.append(day_min['RWS'].iloc[6])
                    diff = (first_value[i] - last_value[i])*100
                    if last_value[i] > rwsc:
                        HnRef = last_value[i]
                    elif diff < 0:
                        HnRef = rwsc
                    else:
                        HnRef = last_value[i] 
                    dat.append(HnRef)
                # calculate mean of values from both sensors
                avg = np.mean(dat)   
                # calc kh and return
                return(round(rwsc/avg, 3))
            
            else:
                raise Exception("Treatment not available for selected site")
                
        elif self.site == "DS2_APP10":
            # define rws crop for DS2_8C
            rwsc = self.rwsc(1858, 1)
            if treatment == 4 or treatment == 5:
                # set specific field capacity per sensor
                fc = [float(self.fc(1858, 1)), float(self.fc(1858, 2))]
                # store data from soilmoisture sensors that are used for swbm in pedralba
                data = []
                data.append(self.get_aggregata_sm("2163", "SoilMoisture1"))
                data.append(self.get_aggregata_sm("2163", "SoilMoisture2"))
                # calculate relative water status based on values from both sensors
                dat = []
                last_value = []
                first_value = []
                for i in range(0, len(data)):
                    # rename column to make iteration possible
                    data[i] = data[i].rename(columns = {data[i].columns[1] : "SoilMoisture"})
                    # calculate relative water status
                    data[i]['RWS'] = data[i]['SoilMoisture']/fc[i]
                    # get min per day
                    day_min = data[i].groupby(data[i].Timestamp.dt.date).min()
                    # calculate HnRef
                    first_value.append(day_min['RWS'].iloc[0])
                    last_value.append(day_min['RWS'].iloc[6])
                    diff = (first_value[i] - last_value[i])*100
                    if last_value[i] > rwsc:
                        HnRef = last_value[i]
                    elif diff < 0:
                        HnRef = rwsc
                    else:
                        HnRef = last_value[i] 
                    dat.append(HnRef)
                # calculate mean of values from both sensors
                avg = np.mean(dat)   
                # calc kh and return
                return(round(rwsc/avg, 3))
            
            else:
                raise Exception("Treatment not available for selected site")
                
        else:
            raise Exception("No SWBM available for selected site")
      
#     def historic_KETo(self):
#         '''
#         function to return historic ETo coefficient values from look up table
#         The ETo coefficient is derived from historic data by dividing Eto values of acutal week with values from previous week 
#         Is done here on daily basis to be more flexible with range of days selected for irrigation recommendation
        
#         Raises
#         ------
#         error if data is not available for selected site

#         Returns
#         -------
#         historic ETo coefficient value per day depending on week
#         '''
#         try:
#             fields = ['Week', 'ETo']
#             ETo_data = pd.read_csv("coefficients/"+ self.site +"_historic_eto.csv", usecols=fields)
#             #iterating over the dates to assign Kc per day depending on month
#             daterange = pd.date_range(start = self.start_day(), 
# 								  end = self.end_day())
#             ETo = []
#             for d in daterange:
#                 week = d.week
#                 for i in range (0, 51):
#                     if i+1 == week:
#                         ETod = ETo_data["ETo"][i]
#                         ETo.append(ETod)
#             return(np.average(ETo))
        
#     # print error message if failing
#         except Exception as e:
#                 	print(e)

    def historic_KETo(self):
        '''
        function to return historic ETo coefficient values from look up table
        The ETo coefficient is derived from historic data by dividing Eto values of acutal week with values from previous week 
        Uses ETo value for week of of first day of timerange
        
        Raises
        ------
        error if data is not available for selected site

        Returns
        -------
        historic ETo coefficient value
        '''
        try:
            fields = ['Week', 'ETo']
            ETo_data = pd.read_csv("coefficients/"+ self.site +"_historic_eto.csv", usecols=fields)

            end_day = self.end_day()
            week = end_day.isocalendar().week
            
            ETo = ETo_data.loc[ETo_data['Week'] == week]["ETo"]
            return(float(ETo.iloc[0]))
        except Exception as e:
                	print(e)
                    
            
    def penman_et_forecast_direct(self):
        '''
        retrieves already calculated penman_56 evaporation forecast values from open-meteo 
        Returns
        -------
        forecast ETo values per day of coming week 
        '''
        # openweather API baseurl
        base = "https://api.open-meteo.com/v1/forecast?"
        
        # specify geolocation
        specurl = "latitude="+self.latitude()+"&longitude="+self.longitude() 
        # set start/end_day
        start_day = '&start_date=' + str(self.end_day() + datetime.timedelta(days = 1))
        end_day = '&end_date=' + str(self.end_day() + datetime.timedelta(days = 7))

        # fetch open forecast
        url = (base + specurl + 
               start_day + 
               end_day+
              '&daily=et0_fao_evapotranspiration&timeformat=unixtime&timezone=Etc/UTC')
              #'&hourly=et0_fao_evapotranspiration&timeformat=unixtime') 
        response = requests.get(url = url)
        # extract daily information as pd.df
        values_df = response.json()
        eto  = pd.DataFrame(values_df['daily'])
        eto.time = pd.to_datetime(eto['time'], unit='s', utc = True)
        eto.time = eto["time"].dt.date
        eto = eto.set_index('time')
        # return values per day
        return eto
    
    def penman_et_openmeteo(self):
        '''
        retrieves penman_56 evaporation forecast values from open-meteo 
        Returns
        -------
        forecast Et values per day
        for week before, could replace oen weather station at sites
        '''
        # openweather API baseurl
        base = "https://api.open-meteo.com/v1/forecast?"
        
        # specify geolocation
        specurl = "latitude="+self.latitude()+"&longitude="+self.longitude() 
        # set start/end_day
        start_day = '&start_date=' + str(self.start_day())
        end_day = '&end_date=' + str(self.end_day())

        # fetch open forecast
        url = (base + specurl + 
               start_day + 
               end_day+
              '&daily=et0_fao_evapotranspiration&timeformat=unixtime&timezone=Etc/UTC')
              #'&hourly=et0_fao_evapotranspiration&timeformat=unixtime') 
        response = requests.get(url = url)
        # extract daily information as pd.df
        values_df = response.json()
        eto  = pd.DataFrame(values_df['daily'])
        eto.time = pd.to_datetime(eto['time'], unit='s', utc = True)
        eto.time = eto["time"].dt.date
        eto = eto.set_index('time')
        # return values per day
        return eto
    

    def penman_et_ecmwf(self):
        '''
        calculates ET based on Penman-Monteith
        input values are derived from openmeteo and ecmwf 
    
        Returns
        -------
        forecast ETo values per day of coming week 
        
        #OWM reports wind speed at 10m height, so need to convert to 2m:
               data["wind_speed"] = data["wind_speed"] * (4.87 / math.log((67.8 * 10) - 5.42))
        
        '''
        base = "https://api.open-meteo.com/v1/forecast?"
        base_ecmwf = "https://api.open-meteo.com/v1/ecmwf?"
    
        # get  temperature from ecmfw
        specurl = "latitude="+self.latitude()+"&longitude="+self.longitude() 
        # set start/end_day
        start_day = '&start_date=' + str(self.end_day() + datetime.timedelta(days = 1))
        end_day = '&end_date=' + str(self.end_day() + datetime.timedelta(days = 7))
    
        # fetch open forecast
        url = (base_ecmwf + specurl + 
               start_day + 
               end_day +
               '&hourly=temperature_2m&timeformat=unixtime')
              #'&hourly=temperature_2m&timeformat=unixtime&timezone=Etc/UTC')
        
        response = requests.get(url = url)
        # extract information as pd.df
        values_df = response.json()
        temp  = pd.DataFrame(values_df['hourly'])
        temp.time = pd.to_datetime(temp['time'], unit='s', utc = True)
        
        # group by day(mean) and add min and max temp values per day
        tmean = temp.groupby(temp.time.dt.date).mean(numeric_only = True)
        tmin = temp.groupby(temp.time.dt.date).min()
        tmax = temp.groupby(temp.time.dt.date).max()
        
        # get wind
        # fetch open forecast
        url = (base_ecmwf + specurl + 
               start_day + 
               end_day +
               '&hourly=windspeed_10m&windspeed_unit=ms&timeformat=unixtime')
               #'&hourly=windspeed_10m&windspeed_unit=ms&timeformat=unixtime&timezone=Etc/UTC')
        response = requests.get(url = url)
        # extract information as pd.df
        values_df = response.json()
        var  = pd.DataFrame(values_df['hourly'])
        var.time = pd.to_datetime(var['time'], unit='s', utc = True)
        # group by day(mean)
        wind = var.groupby(var.time.dt.date).mean(numeric_only = True)
        wind = wind * (4.87 / math.log((67.8 * 10) - 5.42))
        
        # get humidity
        # fetch open forecast
        url = (base_ecmwf + specurl + 
               start_day + 
               end_day +
               '&hourly='+'relativehumidity_2m'+'&timeformat=unixtime')
              #'&hourly='+variable+'&timeformat=unixtime&timezone=Etc/UTC')
        response = requests.get(url = url)
        # extract information as pd.df
        values_df = response.json()
        hum  = pd.DataFrame(values_df['hourly'])
        hum.time = pd.to_datetime(hum['time'], unit='s', utc = True)
        
        # group by day       
        hum_mean = hum.groupby(hum.time.dt.date).mean(numeric_only = True)
        hum_min = hum.groupby(hum.time.dt.date).min()
        hum_max = hum.groupby(hum.time.dt.date).max()
        
        # get radiation
        # fetch open forecast
        url = (base + specurl + 
               start_day + 
               end_day+
              '&daily=shortwave_radiation_sum&timeformat=unixtime&timezone=Etc/UTC')
        response = requests.get(url = url)
        # extract information as pd.df
        values_df = response.json()
        var  = pd.DataFrame(values_df['daily'])
        var.time = pd.to_datetime(var['time'], unit='s', utc = True)
        # group by day(mean)
        rad = var.groupby(var.time.dt.date).mean(numeric_only = True)
        
        # calculate ET0 after FAO56
        # get lat
        lat = float(self.latitude())*np.pi/180  # Latitude of the meteorological station, converting from degrees to radians
        # get elevation
        elevation = float(self.altitude())  # meters above sea-level
        # store needed data in one df
        meteo = pd.DataFrame({"time":tmean.index, 
                              "tmean":tmean.temperature_2m, 
                              "tmax":tmax.temperature_2m, 
                              "tmin":tmin.temperature_2m, 
                              "rh_mean":hum_mean.relativehumidity_2m,
                              "rh_min":hum_min.relativehumidity_2m,
                              "rh_max":hum_max.relativehumidity_2m,                              
                              "wind":wind.windspeed_10m, 
                              "rs":rad.shortwave_radiation_sum}) # in MJ/m²
        # set datetime index
        meteo.index = pd.to_datetime(meteo.time)
    
        # get single objects to pass to function
        time, tmean, tmax, tmin, rh_mean, rh_min, rh_max, wind, rs = [meteo[col] for col in meteo.columns]
    
        # Estimate evapotranspiration 
        penman_et = pyet.pm_fao56(tmean, wind, rs=rs, elevation=elevation,
                                        lat=lat, tmax=tmax, tmin=tmin, 
                                        rh =rh_mean)# rhmin = rh_min, rhmax = rh_max)
        return(pd.DataFrame(penman_et))

    
    def et_wf(self, Et = 'penman'):
        '''
        Parameters
        ----------
        Et : formula for evapotranspiration calculation, must be hargreaves or penman
            DESCRIPTION. The default is 'penman'.

        Raises
        ------
        Exception
            for unknown et values

        Returns
        -------
        forecast et values 
        '''
        
        if Et == "hargreaves":            
            et_wf = self.hargreaves_et_forecast().sum().iloc[0]/self.hargreaves_et().sum().iloc[0]
            return(et_wf)
        elif Et == "penman":
            et_wf = self.penman_forecast().sum(numeric_only=True).iloc[0]/self.penman_et().sum().iloc[0]
            return(et_wf)
        else:
            raise Exception("Unknown ETo formula")
        
    def swbm4(self):
        '''
        Soil water balance model using historic ETo values (treatment = 4)
        currently only available with hargreaves eto from look up table

        Returns
        -------
        irrigation needs of following week as correction factor for irrigation 
        schedule of previous week
        '''

        k = self.factor_fh(4)*self.historic_KETo()
        return(k)
        
    def swbm5(self, Et = "penman"):
        '''
        Soil water balance model using ETo values from forecast (treatment = 5)
        
        Parameters
        ----------
        et : TYPE, optional
            DESCRIPTION. The default is "hargreaves", second option is "penman"

        Raises
        ------
        Exception if unknown et parameter is given

        Returns
        -------
        irrigation needs of following week as correction factor for irrigation 
        schedule of previous week
        '''
        et_wf = self.et_wf(Et = Et)
        k = self.factor_fh(5)*et_wf
        return(k)

# examples

'''
start_date = datetime.date(2024, 1, 2)
end_date = datetime.date(2024, 1, 8)

start_date = datetime.date(2024, 1, 9)
end_date = datetime.date(2024, 1, 15)

start_date = datetime.date(2024, 1, 16)
end_date = datetime.date(2024, 1, 22)

start_date = datetime.date(2024, 1, 23)
end_date = datetime.date(2024, 1, 29)

start_date = datetime.date(2024, 1, 30)
end_date = datetime.date(2024, 2, 5)

start_date = datetime.date(2024, 2, 27)
end_date = datetime.date(2024, 3, 4)

DS1A = dst("DS1A", start_date, end_date)
DS1A.swbm4() 
DS1A.swbm5()
DS1A.FAO_single()

DS1A.factor_fh(4)
DS1A.factor_fh(5)
df = DS1A.get_climate_data()
DS1A.historic_KETo()
DS1A.penman_et()
DS1A.penman_et().sum()
DS1A.penman_forecast()
DS1A.penman_et_forecast_direct().sum(numeric_only = True)
DS1A.et_wf(Et = "penman")


DS5 = dst("DS5", start_date, end_date)
DS5.swbm4()
DS5.factor_fh(4)
DS5.historic_KETo()
df = DS5.get_climate_data()

DSpilot = dst("olivepilot", start_date, end_date)
DSpilot.swbm4()
DSpilot.factor_fh(4)
DSpilot.historic_KETo()
DSpilot.FAO_single()
DSpilot.penman_et()

citruspilot = dst("citruspilot", start_date, end_date)
citruspilot.swbm4()
citruspilot.factor_fh(4)
citruspilot.historic_KETo()
citruspilot.FAO_single()

DS4 = dst("DS4")
DS4.penman_et_openmeteo()
DS4.FAO_single(Et = "openmeteo")


DS7 = dst("DS7")
DS7.penman_et_openmeteo()
DS7.penman_et_ecmwf()
DS7.FAO_single(Et = "openmeteo")
DS7.FAO_single(Et = "ecmwf")
'''

