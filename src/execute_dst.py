# -*- coding: utf-8 -*-
"""
Created on Thu Jun 15 14:39:34 2023

@author: thomasf

Script to execute the Handywater DST and send the results to the DMP

"""

import datetime
import requests
import pandas as pd
from datetime import date, timedelta
import io
from DST import *


# DS1A

end_date = date.today() - timedelta(days=1)
start_date = date.today() - timedelta(days = 7)


try:
    site = "DS1A"
    # get info about installed irrigation system from look up table 
    fields = ['Site', 'Spacing', 'Dripper', 'Flowrate']
    irri_specs = pd.read_csv('Sites.csv', usecols=fields)
    
    # define condition
    condition = (irri_specs['Site'] == site)  
    
    # Use the condition to retrieve the value from column_B
    spacing = float(irri_specs.loc[condition, 'Spacing'].values[0])
    dripper = float(irri_specs.loc[condition, 'Dripper'].values[0])
    flowrate = float(irri_specs.loc[condition, 'Flowrate'].values[0])
    
    # dev id
    dev_id = "fe54b3bec2fa4"
    dst_site = dst(site, start_date = start_date, end_date = end_date)
    irri_rec = pd.DataFrame(index = [0], data = 
                            {"datetime":pd.to_datetime(str(dst_site.end_day()) + " 12:00:00"),
                              "FAO_1D_single":dst_site.FAO_single(),
                              # in m³/ha
                              "FAO_1D_single_ha":dst_site.FAO_single()*10,
                              # in liter/plant (spacing 2x5 m)
                              "FAO_1D_single_plant":dst_site.FAO_single()*spacing,
                              # in hours (n drippers * flow rate)
                              "FAO_1D_single_hours":dst_site.FAO_single()*spacing/(dripper*flowrate),
                              "FAO_1D_dual":dst_site.FAO_dual(),
                              # in liter/ha
                              "FAO_1D_dual_ha":dst_site.FAO_dual()*10,
                              # in liter/plant
                              "FAO_1D_dual_plant":dst_site.FAO_dual()*spacing,
                              # in hours
                              "FAO_1D_dual_hours":dst_site.FAO_dual()*spacing/(dripper*flowrate),
                              "swbm4":dst_site.swbm4(),
                              "swbm5":dst_site.swbm5()
                              })
    # round
    irri_rec = round(irri_rec, 2)
    # store in buffer for sending
    df_buffer = io.StringIO()
    irri_rec.to_csv(df_buffer, index = False, header = False)
    # set pointer to 0
    df_buffer.seek(0)        
    # Create a file-like object with data
    files = {'file': (df_buffer)}
    # send file to DMP via https adress
    posturl = 'https://logger-worker.intranet.ufz.de/gateway/upload/device/' + dev_id
    response = requests.post(posturl, files=files)
    # print response status 
    print(site, response.text)
except Exception as e:
    print(site, e)
    pass


# sites with swbm4 with loop
study_sites = {
     'DS5': '96f71ecec4deb',
     'olivepilot': 'c8d4953d8ba6c',
     'citruspilot': '1c9241cd044a0'
      }

# execute irrigation recommendation calculation and send it to DMP for APP 
for site, dev_id in study_sites.items():
    # get info about installed irrigation system from look up table 
    fields = ['Site', 'Spacing', 'Dripper', 'Flowrate']
    irri_specs = pd.read_csv('Sites.csv', usecols=fields)
    
    # define condition
    condition = (irri_specs['Site'] == site)  
    
    # Use the condition to retrieve the value from column_B
    spacing = float(irri_specs.loc[condition, 'Spacing'].values[0])
    dripper = float(irri_specs.loc[condition, 'Dripper'].values[0])
    flowrate = float(irri_specs.loc[condition, 'Flowrate'].values[0])
    
    try:
        dst_site = dst(site, start_date = start_date, end_date = end_date)
        irri_rec = pd.DataFrame(index = [0], data = 
                                {"datetime":pd.to_datetime(str(dst_site.end_day()) + " 12:00:00"),
                                  "FAO_1D_single":dst_site.FAO_single(),
                                  # in liter/ha
                                  "FAO_1D_single_ha":dst_site.FAO_single()*10,
                                  # in liter/plant (spacing 2x5 m)
                                  "FAO_1D_single_plant":dst_site.FAO_single()*spacing,
                                  # in hours (n drippers * flow rate)
                                  "FAO_1D_single_hours":dst_site.FAO_single()*spacing/(dripper*flowrate),
                                  "FAO_1D_dual":dst_site.FAO_dual(),
                                  # in liter/ha
                                  "FAO_1D_dual_ha":dst_site.FAO_dual()*10,
                                  # in liter/plant
                                  "FAO_1D_dual_plant":dst_site.FAO_dual()*spacing,
                                  # in hours
                                  "FAO_1D_dual_hours":dst_site.FAO_dual()*spacing/(dripper*flowrate),
                                  "swbm4":dst_site.swbm4(),
                                  })
        # round 
        irri_rec = round(irri_rec, 2)
        # store in buffer for sending
        df_buffer = io.StringIO()
        irri_rec.to_csv(df_buffer, index=False, header = False)
        # set pointer to 0
        df_buffer.seek(0)        
        # Create a file-like object with data
        files = {'file': (df_buffer)}
        # send file to DMP via https adress
        posturl = 'https://logger-worker.intranet.ufz.de/gateway/upload/device/' + dev_id
        response = requests.post(posturl, files=files)
        # print response status 
        print(site, response.text)
    except Exception as e:
        print(site, e)
        pass


# sites without swbm as loop
study_sites = {
     'DS1B': 'e02883551783d',
     'DS2_APP10': '692fbdb010795',
     'DS2_APP5': '32951b8e99f86',
     'DS2_7' : '0a6f6cdf21753', 
     'DS2_8C' : 'e87f93a3c21c8', 
     #'DS3': '55895234e1588',
      }

# execute irrigation recommendation calculation and send it to DMP for APP 
for site, dev_id in study_sites.items():
    # get info about installed irrigation system from look up table 
    fields = ['Site', 'Spacing', 'Dripper', 'Flowrate']
    irri_specs = pd.read_csv('Sites.csv', usecols=fields)
    
    # define condition
    condition = (irri_specs['Site'] == site)  
    
    # Use the condition to retrieve the value from column_B
    spacing = float(irri_specs.loc[condition, 'Spacing'].values[0])
    dripper = float(irri_specs.loc[condition, 'Dripper'].values[0])
    flowrate = float(irri_specs.loc[condition, 'Flowrate'].values[0])
    
    try:
        dst_site = dst(site, start_date = start_date, end_date = end_date)
        irri_rec = pd.DataFrame(index = [0], data = 
                                {"datetime":pd.to_datetime(str(dst_site.end_day()) + " 12:00:00"),
                                  "FAO_1D_single":dst_site.FAO_single(),
                                  # in liter/ha
                                  "FAO_1D_single_ha":dst_site.FAO_single()*10,
                                  # in liter/plant (spacing 2x5 m)
                                  "FAO_1D_single_plant":dst_site.FAO_single()*spacing,
                                  # in hours (n drippers * flow rate)
                                  "FAO_1D_single_hours":dst_site.FAO_single()*spacing/(dripper*flowrate),
                                  "FAO_1D_dual":dst_site.FAO_dual(),
                                  # in liter/ha
                                  "FAO_1D_dual_ha":dst_site.FAO_dual()*10,
                                  # in liter/plant
                                  "FAO_1D_dual_plant":dst_site.FAO_dual()*spacing,
                                  # in hours
                                  "FAO_1D_dual_hours":dst_site.FAO_dual()*spacing/(dripper*flowrate),
                                  })
        # round 
        irri_rec = round(irri_rec, 2)
        # store in buffer for sending
        df_buffer = io.StringIO()
        irri_rec.to_csv(df_buffer, index=False, header = False)
        # set pointer to 0
        df_buffer.seek(0)        
        # Create a file-like object with data
        files = {'file': (df_buffer)}
        # send file to DMP via https adress
        posturl = 'https://logger-worker.intranet.ufz.de/gateway/upload/device/' + dev_id
        response = requests.post(posturl, files=files)
        # print response status 
        print(site, response.text)
    except Exception as e:
        print(site, e)
        pass


# trial: sites without any sensors

study_sites = {
     'DS7': '596a300ba7a77',
     'DS3': '55895234e1588',
      }

# execute irrigation recommendation calculation and send it to DMP for APP 
for site, dev_id in study_sites.items():
    # get info about installed irrigation system from look up table 
    fields = ['Site', 'Spacing', 'Dripper', 'Flowrate']
    irri_specs = pd.read_csv('Sites.csv', usecols=fields)
    
    # define condition
    condition = (irri_specs['Site'] == site)  
    
    # Use the condition to retrieve the value from column_B
    spacing = float(irri_specs.loc[condition, 'Spacing'].values[0])
    dripper = float(irri_specs.loc[condition, 'Dripper'].values[0])
    flowrate = float(irri_specs.loc[condition, 'Flowrate'].values[0])
    
    try:
        dst_site = dst(site, start_date = start_date, end_date = end_date)
        irri_rec = pd.DataFrame(index = [0], data = 
                                {"datetime":pd.to_datetime(str(dst_site.end_day()) + " 12:00:00"),
                                  "FAO_1D_single":dst_site.FAO_single(Et = "ecmwf"),
                                  # in liter/ha
                                  "FAO_1D_single_ha":dst_site.FAO_single(Et = "ecmwf")*10,
                                  # in liter/plant (spacing 2x5 m)
                                  "FAO_1D_single_plant":dst_site.FAO_single(Et = "ecmwf")*spacing,
                                  # in hours (n drippers * flow rate)
                                  "FAO_1D_single_hours":dst_site.FAO_single(Et = "ecmwf")*spacing/(dripper*flowrate),
                                  "FAO_1D_dual":dst_site.FAO_dual(Et = "ecmwf"),
                                  # in liter/ha
                                  "FAO_1D_dual_ha":dst_site.FAO_dual(Et = "ecmwf")*10,
                                  # in liter/plant
                                  "FAO_1D_dual_plant":dst_site.FAO_dual(Et = "ecmwf")*spacing,
                                  # in hours
                                  "FAO_1D_dual_hours":dst_site.FAO_dual(Et = "ecmwf")*spacing/(dripper*flowrate),
                                  })
        # round 
        irri_rec = round(irri_rec, 2)
        # store in buffer for sending
        df_buffer = io.StringIO()
        irri_rec.to_csv(df_buffer, index=False, header = False)
        # set pointer to 0
        df_buffer.seek(0)        
        # Create a file-like object with data
        files = {'file': (df_buffer)}
        # send file to DMP via https adress
        posturl = 'https://logger-worker.intranet.ufz.de/gateway/upload/device/' + dev_id
        response = requests.post(posturl, files=files)
        # print response status 
        print(site, response.text)
    except Exception as e:
        print(site, e)
        pass