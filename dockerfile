# Specifying the base image
FROM python:3.10

# install needed libraries
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# add the python scripts 
COPY src/ .

# execute
CMD ["python", "-u", "execute_dst.py"]